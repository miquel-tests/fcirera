package org.macias;

import com.itextpdf.io.font.constants.StandardFonts;
import com.itextpdf.kernel.colors.ColorConstants;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.geom.Rectangle;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfReader;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.kernel.pdf.canvas.parser.PdfTextExtractor;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.properties.TextAlignment;
import com.itextpdf.layout.properties.VerticalAlignment;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.macias.utils.Toast;

import java.io.File;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PDFModificationController implements Initializable {

    private File fileToTransform;
    private String filePath;
    private String outDirectoryPath;
    private boolean isFileTypeSelected = false;

    @FXML
    private ComboBox<String> dropDownList;

    @FXML
    private Button openFileChooser;

    @FXML
    private TextField selectedDocument;

    @FXML
    private Button openDirectoryChooser;

    @FXML
    private TextField selectedDirectory;

    @FXML
    private Button generateModifiedPDF;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        assert dropDownList != null;
        assert openFileChooser != null;
        assert selectedDocument != null;
        assert openDirectoryChooser != null;
        assert selectedDirectory != null;
        assert generateModifiedPDF != null;

        dropDownList.getItems().addAll("Certificado de retenciones", "Modelo 145");

        dropDownList.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
            @Override public void changed(ObservableValue<? extends String> selected, String oldSelection, String newSelection) {
                if(newSelection != null){
                    isFileTypeSelected = true;
                }
            }
        });

        cleanPage();
    }

    public void openFileChooser(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();
        File file = fileChooser.showOpenDialog(openFileChooser.getScene().getWindow());
        if(file != null) {
            fileToTransform = file;
            selectedDocument.setText(file.getAbsolutePath());
            filePath = file.getAbsolutePath();
        }
    }

    public void openDirectoryChooser(ActionEvent event) {
        DirectoryChooser directoryChooser = new DirectoryChooser();
        File file = directoryChooser.showDialog(openDirectoryChooser.getScene().getWindow());
        if(file != null) {
            selectedDirectory.setText(file.getAbsolutePath());
            outDirectoryPath = file.getAbsolutePath();
        }
    }

    public void generateModifiedPDF(ActionEvent event) {
        if(!isFileTypeSelected) {
            showToast(event, "Selecciona un tipo de documento.", 2000);
            return;
        }
        if(filePath == null) {
            showToast(event, "Selecciona un documento para modificar.", 2000);
            return;
        }
        if(outDirectoryPath == null) {
            showToast(event, "Selecciona un directorio de destino.", 2000);
            return;
        }

        try {
            manipulatePdf(outDirectoryPath, event);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void manipulatePdf(String dest, ActionEvent event) throws Exception {
        PdfDocument pdfDoc = new PdfDocument(new PdfReader(filePath), new PdfWriter(dest+"\\"+ fileToTransform.getName()));
        Document doc = new Document(pdfDoc);

        for (int i = 1; i <= pdfDoc.getNumberOfPages(); i++) {

            Rectangle pageSize = pdfDoc.getPage(i).getPageSize();
            float x = 40;
            float y = pageSize.getBottom() + 20;

            String page = PdfTextExtractor.getTextFromPage(pdfDoc.getPage(i));

            if(page != null && !page.equals("")) {
                String[] lines = page.split("\n");
                Paragraph header;
                String patternDate = "yyyy-MM-dd";
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat(patternDate);

                String date = simpleDateFormat.format(new Date(System.currentTimeMillis()));

                if(dropDownList.getSelectionModel().getSelectedIndex() == 1 && lines.length > 1){
                    String[] numbers;
                    try{
                        String validPattern =   "[0-9]{12}";
                        Pattern pattern = Pattern.compile(validPattern);
                        Matcher matcher;

                        for(String line: lines){
                            matcher = pattern.matcher(line);
                            if(matcher.find()){
                                numbers = line.split(" / ");
                                header = new Paragraph("##" + numbers[0].substring(6,12) + "##Modelo 145##"+ date+"##")
                                        .setFont(PdfFontFactory.createFont(StandardFonts.HELVETICA))
                                        .setFontSize(8)
                                        .setFontColor(ColorConstants.BLACK);
                                doc.showTextAligned(header, x, y, i, TextAlignment.LEFT, VerticalAlignment.BOTTOM, 0);
                                break;
                            }
                        }
                    } catch (Exception e) {
                        showToast(event, "Formato de documento no válido en la página " + i, 3000);
                    }

                } else if (dropDownList.getSelectionModel().getSelectedIndex() == 0 && lines.length > 1) {

                    try{
                        String validPattern =   "[0-9]{12}";
                        Pattern pattern = Pattern.compile(validPattern);
                        Matcher matcher;

                        for(String line: lines){
                            line = line.replaceAll("\\s", "");
                            matcher = pattern.matcher(line);
                            if(matcher.find()){
                                header = new Paragraph("##" + line.substring(6,12) + "##Certificado de Retenciones##"+ date+"##")
                                        .setFont(PdfFontFactory.createFont(StandardFonts.HELVETICA))
                                        .setFontSize(8)
                                        .setFontColor(ColorConstants.BLACK);
                                doc.showTextAligned(header, x, y, i, TextAlignment.LEFT, VerticalAlignment.BOTTOM, 0);
                                break;
                            }
                        }
                    } catch (Exception e){
                        cleanPage();
                        showToast(event, "Formato de documento no válido en la página " + i, 3000);
                        return;
                    }
                }
            } else {
                showToast(event, "Página " + i + " no se ha podido leer bien.", 3000);
            }
        }

        if(dropDownList.getSelectionModel().getSelectedIndex() == 1){
            showToast(event, "Modelo 145 Modificado", 2000);
        } else if (dropDownList.getSelectionModel().getSelectedIndex() == 0) {
            showToast(event, "Certificado de Retenciones Modificado", 2000);
        }

        doc.close();
        cleanPage();
    }

    private void cleanPage() {
        selectedDirectory.setText("Ningún directorio de destino seleccionado");
        outDirectoryPath = null;

        selectedDocument.setText("Ningún archivo seleccionado");
        filePath = null;

        dropDownList.getSelectionModel().clearSelection();
        isFileTypeSelected = false;
        dropDownList.setPromptText("Seleccionar tipo de documento");

        fileToTransform = null;
    }

    private void showToast(ActionEvent event, String toastText, int duration) {
        int fadeInTime = 500;
        int fadeOutTime= 500;
        Toast.makeText((Stage)((Node) event.getSource()).getScene().getWindow(), toastText, duration, fadeInTime, fadeOutTime);
    }
}
