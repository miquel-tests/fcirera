package org.macias;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
import org.macias.utils.Toast;
import org.macias.utils.distributionXML;
import org.macias.utils.nominaXML;
import org.macias.utils.projectXML;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.nio.file.*;
import java.text.SimpleDateFormat;
import java.util.*;

public class ZIPGenerationController implements Initializable {

    private String originDirectoryPath;
    private String outDirectoryPath;
    private List<File> nominasNIF;
    private List<File> xmls;

    @FXML
    private TextField NIFTrabajador;

    @FXML
    private TextField IDTrabajador;

    @FXML
    private TextField firstName;

    @FXML
    private TextField lastName;

    @FXML
    private Button originDirectoryChooser;

    @FXML
    private TextField originDirectory;

    @FXML
    private Button outputDirectoryChooser;

    @FXML
    private TextField outputDirectory;

    @FXML
    private Button generateZIP;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        assert NIFTrabajador != null;
        assert IDTrabajador != null;
        assert firstName != null;
        assert lastName != null;
        assert originDirectory != null;
        assert originDirectoryChooser != null;
        assert outputDirectory != null;
        assert outputDirectoryChooser != null;
        assert generateZIP != null;

        cleanPage();
    }

    private void cleanPage() {
        nominasNIF = new ArrayList<>();
        xmls = new ArrayList<>();

        originDirectory.setText("Ningún directorio origen seleccionado");
        originDirectoryPath = null;

        outputDirectory.setText("Ningún directorio de destino seleccionado");
        outDirectoryPath = null;

        NIFTrabajador.setText("");
        NIFTrabajador.setPromptText("Introduce el NIF del trabajador");

        IDTrabajador.setText("");
        IDTrabajador.setPromptText("Introduce el # ID del trabajador");

        firstName.setText("");
        firstName.setPromptText("Introduce el nombre del trabajador");

        lastName.setText("");
        lastName.setPromptText("Introduce el apellido del trabajador");
    }

    public void openOriginDirectoryChooser(ActionEvent actionEvent) {
        DirectoryChooser directoryChooser = new DirectoryChooser();
        File file = directoryChooser.showDialog(originDirectoryChooser.getScene().getWindow());
        if (file != null) {
            originDirectory.setText(file.getAbsolutePath());
            originDirectoryPath = file.getAbsolutePath();
        }
    }

    public void openOutputDirectoryChooser(ActionEvent actionEvent) {
        DirectoryChooser directoryChooser = new DirectoryChooser();
        File file = directoryChooser.showDialog(outputDirectoryChooser.getScene().getWindow());
        if (file != null) {
            outputDirectory.setText(file.getAbsolutePath());
            outDirectoryPath = file.getAbsolutePath();
        }
    }

    public void generateZIP(ActionEvent event) {
        if(!NIFTrabajador.getText().matches("[0-9]{8}[A-Z]{1}")) {
            showToast(event, "Formato NIF Incorrecto.", 2000);
            return;
        }
        if(!IDTrabajador.getText().matches("[0-9]{6}")) {
            showToast(event, "Formato # ID Incorrecto.", 2000);
            return;
        }
        if(firstName.getText().equals("")) {
            showToast(event, "Falta introducir el nombre del trabajador", 2000);
            return;
        }
        if(lastName.getText().equals("")) {
            showToast(event, "Falta introducir el apellido del trabajador", 2000);
            return;
        }
        if(originDirectoryPath == null) {
            showToast(event, "Selecciona un directorio origen.", 2000);
            return;
        }
        if(outDirectoryPath == null) {
            showToast(event, "Selecciona un directorio de destino.", 2000);
            return;
        } else {
            outDirectoryPath = outDirectoryPath + "\\" + "historicalData_" + NIFTrabajador.getText();
            File theDir = new File(outDirectoryPath);
            if (!theDir.exists()){
                theDir.mkdirs();
            }
        }

        searchForNominasNIF(new File(originDirectoryPath));
        if (nominasNIF.size() > 0) {
            createZIP(event);
            createProjectZIP(event);
        } else {
            showToast(event, "No se han encontrado nóminas con este NIF", 2500);
        }

    }

    private void createProjectZIP(ActionEvent event) {
        Map<String, String> env = new HashMap<>();
        env.put("create", "true");

        SimpleDateFormat format = new SimpleDateFormat("yyyy_MM");
        String dateString = format.format(new Date(System.currentTimeMillis()));

        String path = "jar:file:/" + outDirectoryPath + "\\" + NIFTrabajador.getText() + ".zip";

        URI uri = URI.create(path.replace("\\", "/"));

        String nameXML =  NIFTrabajador.getText() + "_" + dateString;

        searchForProjectXml(new File(outDirectoryPath), nameXML);
        searchForZip(new File(outDirectoryPath));

        try (FileSystem zipfs = FileSystems.newFileSystem(uri, env)) {
            for(File xml: xmls) {
                Path xmlFile = Paths.get(xml.getAbsolutePath());
                Path pathInZipfile = zipfs.getPath(xml.getName());

                Files.copy(xmlFile, pathInZipfile, StandardCopyOption.REPLACE_EXISTING);
            }
        } catch (IOException e) {
            showToast(event, "Error al generar el ZIP", 2500);
            e.printStackTrace();
        }

        for(File xml: xmls) {
            if(!xml.delete()) {
                showToast(event, "Problemas al eliminar xml!", 2500);
            }
        }

        showToast(event, "ZIP generado correctamente!", 2500);
        cleanPage();
    }

    private void searchForNominasNIF(File root) {
        if(root == null || nominasNIF == null) return;
        if(root.isDirectory()) {
            for(File file : Objects.requireNonNull(root.listFiles())) {
                searchForNominasNIF(file);
            }
        } else if(root.isFile() && root.getName().contains(NIFTrabajador.getCharacters())) {
            nominasNIF.add(root);
        }
    }

    private void searchForXml(File root) {
        if(root == null || nominasNIF == null) return;
        if(root.isDirectory()) {
            for(File file : Objects.requireNonNull(root.listFiles())) {
                searchForXml(file);
            }
        } else if(root.isFile() && root.getName().contains(NIFTrabajador.getCharacters()) && !root.getName().contains(".zip")) {
            xmls.add(root);
        }
    }

    private void searchForProjectXml(File root, String nameXML) {
        if(root == null) return;
        if(root.isDirectory()) {
            for(File file : Objects.requireNonNull(root.listFiles())) {
                searchForProjectXml(file, nameXML);
            }
        } else if(root.isFile() && root.getName().contains(nameXML)) {
            xmls.add(root);
        } else if(root.isFile() && root.getName().contains("project.xml")) {
            xmls.add(root);
        }
    }

    private void searchForZip(File root) {
        if(root == null) return;
        if(root.isDirectory()) {
            for(File file : Objects.requireNonNull(root.listFiles())) {
                searchForZip(file);
            }
        } else if(root.isFile() && (root.getName().contains(".zip") && (root.getName().contains("_")) && root.getName().contains(NIFTrabajador.getText()))) {
            xmls.add(root);
        }
    }

    private void createZIP(ActionEvent event) {
        Map<String, String> env = new HashMap<>();
        env.put("create", "true");

        SimpleDateFormat format = new SimpleDateFormat("yyyy_MM");
        String dateString = format.format(new Date(System.currentTimeMillis()));

        String path = "jar:file:/" + outDirectoryPath + "\\" + NIFTrabajador.getText() + "_" + dateString + ".zip";

        URI uri = URI.create(path.replace("\\", "/"));
        String nameXML =  NIFTrabajador.getText() + "_" + dateString;

        for(File nomina:nominasNIF){
            try {
                nominaXML.createXML(outDirectoryPath, nomina.getName(), IDTrabajador.getText(), firstName.getText(), lastName.getText());
            } catch (TransformerException e) {
                e.printStackTrace();
            } catch (ParserConfigurationException e) {
                e.printStackTrace();
            }
        }

        searchForXml(new File(outDirectoryPath));

        try {
            distributionXML.createXML(outDirectoryPath, IDTrabajador.getText(), nameXML);
            projectXML.createXML(outDirectoryPath, IDTrabajador.getText());
        } catch (TransformerException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }

        try (FileSystem zipfs = FileSystems.newFileSystem(uri, env)) {
            for(File nomina: nominasNIF) {
                Path nominaFile = Paths.get(nomina.getAbsolutePath());
                Path pathInZipfile = zipfs.getPath(nomina.getName());

                Files.copy(nominaFile, pathInZipfile, StandardCopyOption.REPLACE_EXISTING);
            }
            for(File xml: xmls) {
                Path nominaFile = Paths.get(xml.getAbsolutePath());
                Path pathInZipfile = zipfs.getPath(xml.getName());

                Files.copy(nominaFile, pathInZipfile, StandardCopyOption.REPLACE_EXISTING);
            }
        } catch (IOException e) {
            showToast(event, "Error al generar el ZIP", 2500);
            e.printStackTrace();
        }

        for(File xml: xmls) {
            if(!xml.delete()) {
                showToast(event, "Problemas al eliminar xml!", 2500);
            }
        }

        xmls = new ArrayList<>();
    }

    private void showToast(ActionEvent event, String toastText, int duration) {
        int fadeInTime = 500;
        int fadeOutTime= 500;
        Toast.makeText((Stage)((Node) event.getSource()).getScene().getWindow(), toastText, duration, fadeInTime, fadeOutTime);
    }
}