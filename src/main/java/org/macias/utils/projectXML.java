package org.macias.utils;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;

public class projectXML {

    public static final String XML_PROJECT_TAG = "project";

    public static final String XML_TITLE_TAG = "title";
    public static final String XML_PAPER_CONF_TAG = "paper_conf_id";
    public static final String XML_REGROUP_PAPER_TAG = "regroup_paper";
    public static final String XML_NOTIFICATION_DELAY_TAG = "notification_delay";

    public static void createXML(
            String outputDirectory,
            String ID)                throws TransformerException, ParserConfigurationException {


        DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();

        DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();

        Document document = documentBuilder.newDocument();

        Element root = document.createElement(XML_PROJECT_TAG);
        root.setAttribute("version", "1.0");
        root.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
        document.appendChild(root);

        // titleElement element
        Element titleElement = document.createElement(XML_TITLE_TAG);
        titleElement.appendChild(document.createTextNode("Histórico de nóminas"));
        root.appendChild(titleElement);

        // paperConf element
        Element paperConfElement = document.createElement(XML_PAPER_CONF_TAG);
        paperConfElement.setAttribute("type", "code");
        paperConfElement.appendChild(document.createTextNode("no_paper"));
        root.appendChild(paperConfElement);

        // regroup paper element
        Element regroupPaperElement = document.createElement(XML_REGROUP_PAPER_TAG);
        regroupPaperElement.appendChild(document.createTextNode("true"));
        root.appendChild(regroupPaperElement);

        // notificationDelay element
        Element notificationDelayElement = document.createElement(XML_NOTIFICATION_DELAY_TAG);
        notificationDelayElement.appendChild(document.createTextNode("0"));
        root.appendChild(notificationDelayElement);


        // create the xml file
        //transform the DOM Object to an XML File
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        transformerFactory.setAttribute("indent-number", 2);
        Transformer transformer = transformerFactory.newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        DOMSource domSource = new DOMSource(document);

        String path = outputDirectory + "/" + "project.xml";
        File xml = new File(path.replace("\\", "/"));
        StreamResult streamResult = new StreamResult(xml);

        transformer.transform(domSource, streamResult);
    }
}
