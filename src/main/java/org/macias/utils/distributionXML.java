package org.macias.utils;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;

public class distributionXML {

    public static final String XML_DISTRIBUTION_TAG = "distribution";

    public static final String XML_TITLE_TAG = "title";
    public static final String XML_DISTRIBUTION_TYPE_TAG = "distribution_type_id";

    public static void createXML(String outputDirectory, String ID, String nameXML) throws TransformerException, ParserConfigurationException {

        DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();

        DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();

        Document document = documentBuilder.newDocument();

        // root element
        Element root = document.createElement(XML_DISTRIBUTION_TAG);
        root.setAttribute("version", "1.0");
        root.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
        document.appendChild(root);

        // userDataElement element
        Element userDataElement = document.createElement(XML_TITLE_TAG);
        userDataElement.appendChild(document.createTextNode("Nóminas"));
        root.appendChild(userDataElement);

        // firstname element
        Element distributionTypeElement = document.createElement(XML_DISTRIBUTION_TYPE_TAG);
        distributionTypeElement.setAttribute("type", "code");
        distributionTypeElement.appendChild(document.createTextNode("historical_data"));
        root.appendChild(distributionTypeElement);


        // create the xml file
        //transform the DOM Object to an XML File
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        transformerFactory.setAttribute("indent-number", 2);
        Transformer transformer = transformerFactory.newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        DOMSource domSource = new DOMSource(document);

        String path = outputDirectory + "/" + nameXML + ".xml";
        File xml = new File(path.replace("\\", "/"));
        StreamResult streamResult = new StreamResult(xml);

        transformer.transform(domSource, streamResult);
    }
}
