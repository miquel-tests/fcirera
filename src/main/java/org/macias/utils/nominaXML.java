package org.macias.utils;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;

public class nominaXML {

    public static final String XML_DOCUMENT_TAG = "document";

    public static final String XML_USER_DATA_TAG = "user-data";
    public static final String XML_FIRST_NAME_TAG = "first_name";
    public static final String XML_LAST_NAME_TAG = "last_name";
    public static final String XML_ORGANIZATION_CODE_TAG = "organization_code";
    public static final String XML_REGISTRATION_KEY_TAG = "registration_key";
    public static final String XML_TECHNICAL_ID_TAG = "technical_id";

    public static final String XML_DOCUMENT_DATE_TAG = "document_date";
    public static final String XML_DOCUMENT_TITLE_TAG = "document_title";

    private static final String _organizationCode = "amgen-spain";

    public static void createXML(
            String outputDirectory,
            String nominaName,
            String ID,
            String firstName,
            String lastName)                throws TransformerException, ParserConfigurationException {


        String[] nominaNames = nominaName.split("-");
        String nominaN = nominaName.replace(".pdf", ".xml");

        DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();

        DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();

        Document document = documentBuilder.newDocument();

        // root element
        Element root = document.createElement(XML_DOCUMENT_TAG);
        root.setAttribute("version", "1.0");
        root.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
        document.appendChild(root);

        // userDataElement element
        Element userDataElement = document.createElement(XML_USER_DATA_TAG);
        root.appendChild(userDataElement);

        // firstname element
        Element firstNameElement = document.createElement(XML_FIRST_NAME_TAG);
        firstNameElement.appendChild(document.createTextNode(firstName));
        userDataElement.appendChild(firstNameElement);

        // lastname element
        Element lastname = document.createElement(XML_LAST_NAME_TAG);
        lastname.appendChild(document.createTextNode(lastName));
        userDataElement.appendChild(lastname);

        // organizationCode element
        Element organizationCode = document.createElement(XML_ORGANIZATION_CODE_TAG);
        organizationCode.appendChild(document.createTextNode(_organizationCode));
        userDataElement.appendChild(organizationCode);

        // registrationKey elements
        Element registrationKey = document.createElement(XML_REGISTRATION_KEY_TAG);
        registrationKey.appendChild(document.createTextNode(ID));
        userDataElement.appendChild(registrationKey);

        // technicalID elements
        Element technicalID = document.createElement(XML_TECHNICAL_ID_TAG);
        technicalID.appendChild(document.createTextNode(ID));
        userDataElement.appendChild(technicalID);

        // documentDate elements
        Element documentDate = document.createElement(XML_DOCUMENT_DATE_TAG);
        documentDate.appendChild(document.createTextNode(nominaNames[1].substring(0,4) + "-" + nominaNames[1].substring(4) + "-01"));
        root.appendChild(documentDate);

        // documentTitle eements
        Element documentTitle = document.createElement(XML_DOCUMENT_TITLE_TAG);
        documentTitle.appendChild(document.createTextNode("Nomina de " + firstName + " " + lastName));
        root.appendChild(documentTitle);

        // create the xml file
        //transform the DOM Object to an XML File
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        transformerFactory.setAttribute("indent-number", 2);
        Transformer transformer = transformerFactory.newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        DOMSource domSource = new DOMSource(document);

        String path = outputDirectory + "/" +  nominaN;
        File xml = new File(path.replace("\\", "/"));
        StreamResult streamResult = new StreamResult(xml);

        transformer.transform(domSource, streamResult);
    }
}
