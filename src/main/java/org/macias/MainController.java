package org.macias;

import javafx.fxml.FXML;
import javafx.scene.control.TabPane;

public class MainController {

    @FXML
    private TabPane root;
    @FXML
    private PDFModificationController PDFModificationTabController;
    @FXML
    private ZIPGenerationController ZIPGenerationTabController;
}
